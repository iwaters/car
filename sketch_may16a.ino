#define x A0
#define y A1
#define pwm1 9
#define pwm2 10
#define pwm3 5
#define pwm4 6

void setup() {
  // put your setup code here, to run once:
  pinMode(pwm1, OUTPUT);
  pinMode(pwm2, OUTPUT);
  pinMode(pwm3, OUTPUT);
  pinMode(pwm4, OUTPUT);
}





void loop() {
  // put your main code here, to run repeatedly:
 int xval = analogRead(x);
 int yval= analogRead(y);
 xval >>=1;

  
 
  if(xval > 255)
  {
    digitalWrite(pwm2, 0);
    digitalWrite(pwm4, 0);
    analogWrite(pwm1, (xval - 256));    
    analogWrite(pwm3, (xval - 256));
  }
  
  else 
    if(xval < 255)
    {
      digitalWrite(pwm1, 0);
      digitalWrite(pwm3, 0);
      analogWrite(pwm2, (255 - xval));    
      analogWrite(pwm4, (255 - xval));    
    }
    else {
      digitalWrite(pwm1, 0);
      digitalWrite(pwm2, 0);
      digitalWrite(pwm3, 0);
      digitalWrite(pwm4, 0);      
    }
 
}
